<?php

namespace App\Models;

use CodeIgniter\Model;

class WeatherModel extends Model
{
    protected $table = 'temperatures';

    public function getNews($departement = false)
    {
        if ($departement === false) {
            return $this->findAll();
        }

        return $this->where(['departement' => $departement])->first();
    }
}