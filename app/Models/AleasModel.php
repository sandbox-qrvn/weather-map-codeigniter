<?php

namespace App\Models;

use CodeIgniter\Model;

class AleasModel extends Model
{
    protected $table = 'vigilance_texte';

    public function getAleas($idDepartement = false)
    {
        if ($idDepartement === false) {
            return $this->findAll();
        }

        return $this->where(['id' => $idDepartement])->first();
    }

}