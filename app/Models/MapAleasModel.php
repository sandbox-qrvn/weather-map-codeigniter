<?php

namespace App\Models;

use CodeIgniter\Model;

class MapAleasModel extends Model
{
    protected $table = 'vigilance_carte';

    public function getAleas($idDepartement = false)
    {
        if ($idDepartement === false) {
            return $this->findAll();
        }

        return $this->where(['dep_id' => $idDepartement])->first();
    }

}