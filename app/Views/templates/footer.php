<!-- <em>&copy; 2021</em> -->
</body>

<script src="https://unpkg.com/leaflet@1.9.2/dist/leaflet.js"
     integrity="sha256-o9N1jGDZrf5tS+Ft4gbIK7mYMipq9lqpVJ91xHSyKhg="
     crossorigin=""></script>
     <script type="text/javascript">
        var weatherData = <?php echo json_encode($temperatures); ?>;
        var aleasData = <?php echo json_encode($aleas); ?>;
        var mapAleas = <?php echo json_encode($map_aleas); ?>;
    </script>
<script type = 'text/javascript' src = "<?php echo base_url(); ?>/weather-map-codeigniter/js/script.js"></script>
</html>