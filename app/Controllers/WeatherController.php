<?php

namespace App\Controllers;

use App\Models\AleasModel;
use App\Models\MapAleasModel;
use App\Models\NewsModel;
use App\Models\WeatherModel;

class WeatherController extends BaseController
{
    function __construct()
    {
        /* $this->load->helper('url'); */
    }
    
    public function index()
    {
        /* $model = model(WeatherModel::class);

        $data = [
            'temperatures'  => $model->getNews(),
            'title' => 'Les températures'
        ]; */

        $weatherModel = model(WeatherModel::class);
        $aleasModel = model(AleasModel::class);
        $mapAleasModel = model(MapAleasModel::class);
        

        $data = [
            'temperatures'  => $weatherModel->getNews(),
            'aleas'  => $aleasModel->getAleas(),
            'map_aleas'  => $mapAleasModel->getAleas(),
            'title' => 'Les températures'
        ];

        return view('templates/header', $data)
            . view('weather/overview')
            . view('templates/footer');
    }

    public function view($departement = null)
    {
        // $model = model(WeatherModel::class);
        // $data['temperatures'] = $model->getNews($departement);

        /* if (empty($data['temperatures'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Cannot find the departement item: ' . $departement);
        } */

        // $data['departement'] = $data['temperatures']['departement'];
        // $data['description'] = $data['temperatures']['weather_description'];

        $weatherModel = model(WeatherModel::class);
        $aleasModel = model(AleasModel::class);
        $data['temperatures'] = $weatherModel->getNews($departement);
        $data['aleas'] = $aleasModel->getAleas($departement);

        if (empty($data['temperatures'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Cannot find the departement item: ' . $departement);
        }

        //weatherModel
        $data['departement'] = $data['temperatures']['departement'];
        $data['description'] = $data['temperatures']['weather_description'];
        //aleasModel
        $data['risk_color'] = $data['aleas']['risk_color'];
        $data['text'] = $data['aleas']['text'];

        return view('templates/header', $data)
            . view('weather/view')
            . view('templates/footer');
    }
}